﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class BundleCreate : MonoBehaviour {

    // Use this for initialization
    [MenuItem("Assets/Create bundle")]
    static void Create() {
        if (!Directory.Exists(Application.streamingAssetsPath))
            Directory.CreateDirectory(Application.streamingAssetsPath);
        BuildPipeline.BuildAssetBundles(Application.streamingAssetsPath, BuildAssetBundleOptions.None, EditorUserBuildSettings.activeBuildTarget);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
