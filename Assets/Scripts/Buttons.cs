﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Buttons : MonoBehaviour
{

    [SerializeField]
    private Button[] buttons;
    [SerializeField]
    private Text[] buttonTexts;
    private Action<int> onClickAction;

    private void Start()
    {

    }

    public void SetOnClickAction(Action<int> action)
    {
        onClickAction = action;
    }

    public void OnCilckButton(int index)
    {
        gameObject.SetActive(false);
        onClickAction?.Invoke(index);
    }

    public void SetButtons(string[] texts)
    {
        for (int i = 0; i < texts.Length; i++)
        {
            if (texts.Length == i)
                break;
            buttonTexts[i].text = texts[i]; 
        }

        for (int i = 0; i < buttons.Length; i++)
        {
            if (texts.Length > i)
            {
                buttons[i].gameObject.SetActive(true);
            }
            else
            {
                buttons[i].gameObject.SetActive(false);
            }
        }
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }
}
