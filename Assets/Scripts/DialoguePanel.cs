﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialoguePanel : MonoBehaviour
{
    [SerializeField]
    private Text nameLabel;
    [SerializeField]
    private Text textLabel;

    public void SetName(string name)
    {
        nameLabel.text = name;
    }

    public void SetText(string text)
    {
        textLabel.text = text;
    }
}
