﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryStep 
{
    public int id;
    public int nextId;

    public string characterType;
    public string emotion;
    public string name;
    public string Text;

    public string[] ButtonsTexts;
    public int[] ButtonsGoTo;

    public string song;
    public string place;
}
