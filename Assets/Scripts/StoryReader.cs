﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StoryReader : MonoBehaviour
{
    [SerializeField]
    private AudioSource audioSource;
    [SerializeField]
    private Transform backgroundRoot;

    private Dictionary<int, StoryStep> storySteps = new Dictionary<int, StoryStep>();
    private StoryStep currentStep;

    private Dictionary<string, Character> characters = new Dictionary<string, Character>();

    private Buttons buttons;

    private bool buttonsMode = false;

    public static Dictionary<string, AssetBundle> loadedAssetBundles = new Dictionary<string, AssetBundle>();

    private Character currentCharacter;
    private GameObject currentBackground;

    private void Start()
    {
        LoadStory();
        NextStep(0);
    }

    private void LoadStory()
    {
        AssetBundle assetBundle = GetAssetBandle(Application.streamingAssetsPath + "/story");
        string[] rawSteps = (assetBundle.LoadAsset<TextAsset>("story")).text.Split(new string[] { "\n", "\r\n" }, System.StringSplitOptions.RemoveEmptyEntries);
        foreach (var step in rawSteps)
        {
            StoryStep storyStep = JsonUtility.FromJson<StoryStep>(step);
            storySteps.Add(storyStep.id, storyStep);
        }
    }

    void OnButtonClick(int buttonIndex)
    {
        buttonsMode = false;
        if (currentStep.ButtonsGoTo?.Length > buttonIndex)
        {
            NextStep(currentStep.ButtonsGoTo[buttonIndex]);
        }
    }

    void NextStep(int sceneId)
    {
        if (storySteps.TryGetValue(sceneId, out var storyStep))
        {
            currentStep = storyStep;
            if (!string.IsNullOrEmpty(storyStep.characterType))
            {
                if (characters.TryGetValue(storyStep.characterType, out Character character))
                {
                    if (currentCharacter != character)
                    {
                        currentCharacter.Hide();
                        currentCharacter = character;
                        currentCharacter.Show();
                    }
                }
                else
                {
                    if (currentCharacter != null)
                        currentCharacter.Hide();
                    AssetBundle assetBundle = GetAssetBandle(Application.streamingAssetsPath + "/character/" + storyStep.characterType);
                    var prefab = assetBundle.LoadAsset<GameObject>(storyStep.characterType);
                    GameObject characterGameObject = Instantiate(prefab, transform);
                    currentCharacter = characterGameObject.GetComponentInChildren<Character>();
                    characters.Add(storyStep.characterType, currentCharacter);
                }
            }
            else
            {
                if (currentCharacter == null)
                {
                    return;//ошибка
                }
            }

            

            if (!string.IsNullOrEmpty(storyStep.emotion))
            {
                currentCharacter.SetEmotion((Character.Emotions)Enum.Parse(typeof(Character.Emotions), storyStep.emotion, true));
            }

            if (!string.IsNullOrEmpty(storyStep.name))
            {
                currentCharacter.SetName(storyStep.name);
            }

            if (!string.IsNullOrEmpty(storyStep.Text))
            {
                currentCharacter.SetText(storyStep.Text);
            }

            if (storyStep.ButtonsTexts?.Length > 0)
            {
                if (buttons == null)
                {
                    AssetBundle assetBundle = GetAssetBandle(Application.streamingAssetsPath + "/ui/buttons");
                    var prefab = assetBundle.LoadAsset<GameObject>("buttons");
                    GameObject buttonsGameObject = Instantiate(prefab, transform);
                    buttons = buttonsGameObject.GetComponentInChildren<Buttons>();
                    buttons.SetOnClickAction(OnButtonClick);
                }
                buttons.SetButtons(storyStep.ButtonsTexts);
                buttons.Show();
                buttonsMode = true;
            }
            if (!string.IsNullOrEmpty(storyStep.song))
            {
                if (storyStep.song == "none")
                    audioSource.Stop();
                else
                {
                    AssetBundle assetBundle = GetAssetBandle(Application.streamingAssetsPath + "/music/" + storyStep.song);
                    var clip = assetBundle.LoadAsset<AudioClip>(storyStep.song);
                    audioSource.clip = clip;
                    audioSource.Play();
                }
            }
            if (!string.IsNullOrEmpty(storyStep.place))
            {
                Destroy(currentBackground);
                if (storyStep.place != "none")
                {
                    AssetBundle assetBundle = GetAssetBandle(Application.streamingAssetsPath + "/background/" + storyStep.place);
                    var prefab = assetBundle.LoadAsset<GameObject>(storyStep.place);
                    currentBackground = Instantiate(prefab, backgroundRoot);
                }
            }
        }
        else
        {
            return;//ошибка
        }
    }

    public AssetBundle GetAssetBandle(string path)
    {
        if (!loadedAssetBundles.TryGetValue(path, out AssetBundle assetBundle))
        {
            assetBundle = AssetBundle.LoadFromFile(path);
            loadedAssetBundles.Add(path, assetBundle);
        }
        return assetBundle;
    }

    private void Update()
    {
        if (!buttonsMode)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (currentStep.nextId == -1)
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
                else
                {
                    NextStep(currentStep.nextId);
                }
            }
        }
    }
}
