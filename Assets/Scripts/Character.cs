﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public enum Emotions
    {
        Neutral,
        Angry,
        Happy,
        Horny,
        Sad,
        Shocked,
        Suspicious,
        Vampire,
        Сonfuse
    }

    [SerializeField]
    private Transform emotionsRoot;
    [SerializeField]
    private Transform facesRoot;
    [SerializeField]
    private DialoguePanel dialoguePanel;
    [SerializeField]
    private string characterName = string.Empty;


    private Dictionary<string, Transform> emotions = new Dictionary<string, Transform>();
    private Dictionary<string, Transform> faces = new Dictionary<string, Transform>();

    bool emotionsIsOk = false;

    void Start()
    {
        dialoguePanel.SetName(characterName);
    }

    void PrepareEmotions()
    {
        if (emotionsRoot != null)
        {
            Transform[] emotionsTemp = emotionsRoot.GetComponentsInChildren<Transform>(true);
            for (int i = 0; i < emotionsTemp.Length; i++)
            {
                Transform emotion = emotionsTemp[i];
                if (emotion.name.Contains("Emotion_"))
                    emotions.Add(emotion.name.Replace("Emotion_", ""), emotion);
            }
        }
        if (facesRoot != null)
        {
            Transform[] facesTemp = facesRoot.GetComponentsInChildren<Transform>(true);
            for (int i = 0; i < facesTemp.Length; i++)
            {
                Transform face = facesTemp[i];
                if (face.name.Contains("Face_"))
                    faces.Add(face.name.Replace("Face_", ""), face);
            }
        }
        emotionsIsOk = true;
    }

    public void SetEmotion(Emotions emotion)
    {
        if (!emotionsIsOk)
            PrepareEmotions();

        if (emotions.ContainsKey(emotion.ToString()))
        {
            foreach (var item in emotions)
            {
                if (item.Key == emotion.ToString())
                    item.Value.gameObject.SetActive(true);
                else
                    item.Value.gameObject.SetActive(false);
            }
        }

        if (faces.ContainsKey(emotion.ToString()))
        {
            foreach (var item in faces)
            {
                if (item.Key == emotion.ToString())
                    item.Value.gameObject.SetActive(true);
                else
                    item.Value.gameObject.SetActive(false);
            }
        }
    }

    public void SetName(string characterName)
    {
        this.characterName = characterName;
        dialoguePanel.SetName(characterName);
    }

    public void SetText(string text)
    {
        dialoguePanel.SetText(text);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }
}
